This is a bot that play at 4 in row / power 4

The application is written with angular 7 / Typescript / SCSS

A previous version written in LiveScript can be found at this address https://github.com/bormat/4InArowLibrairy

Demos: http://bormat2.free.fr/wordpress4/

The new version does not work yet on mobile while the old works.
The difficulty slider is also missing.

To open the project in development mode:
`ng serve` and go to http://localhost:4200/

To run tests:
`ng test`


To use a server side pre-rendering with angular universal
`npm run build:ssr && npm run serve:ssr`
