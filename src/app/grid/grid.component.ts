import { Component, OnInit,Output } from '@angular/core';
import ia from './brain/ia'
import ShowHideSwitch from './ShowHideSwitch'

const model = ia.model
const isGameOver = model.isGameOver.bind(model)


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})

export class GridComponent implements OnInit {
  Math = Math
  //internal
  grille : Array<number>;
  _1px = require('./1px.png');
  reload_img = require('./reload.png');
  time = 0
  grilleCreator : Array<number>
  ia = ia;
  keyCode : number;
  model = ia.model;
  threadIsntUsed : boolean = true;
  stackPosition = [];
  isBotActive = true;
  modeCreator = false;
  whyItIsFinish = false;
  animation2 = true;
  popup = new ShowHideSwitch;
  fen = {
    stayOpen: true,
    disp: "play"
  };
  mode = 'normal';
  message = 'ça va commencer';
  endGameMessage = true;
  minLine = 0;
  maxLine = 5;
  minCol = 0;
  maxCol = 6;
  preview:any;
  tabColor = {
    'a': 'rgb(255,50,234)',
    'e': 'rgb(21,90,22)',
    'f': 'rgb(97,1,234)',
    'h': 'rgb(0,200,180)',
    'i': 'rgb(97,28,34)',
    'n': 'rgb(97,234,0)',
    'g': 'rgb(200,200,234)',
    'j': 'rgb(12,20,234)',
    0: 'white',
    1: 'rgb(255,251,0)',
    2: 'rgb(255,0,0)',
    3: 'rgb(205,209,77)',
    4: 'rgb(160,166,0)',
    5: 'rgb(255,162,0)',
    6: 'rgb(99,99,99)',
    7: 'rgb(255,128,128)',
    8: 'rgb(140,0,0)',
    9: 'rgb(0,255,221)'
  }
  animRun :boolean = false;
  displayScroll: string;
  lastY: number;
  replayIconPop : ShowHideSwitch = new ShowHideSwitch(() => {
    return this.replayIcon();
  });
  touches


  constructor() {
    var o = this;
    this.preview = {
      pos: 0,
      add: function(it){
        var i$, i, ref$;
        for (i$ = 0; i$ <= 6; ++i$) {
          i = i$;
          if (!model.grille[(((this.pos += it) % (ref$ = 7) + ref$) % ref$)]) {
            return this.on();
          }
        }
      },
      off: function(){
        var i$, i, results$ = [];
        for (i$ = 0; i$ <= 6; ++i$) {
          i = i$;
          results$.push(o.grille[i] = model.grille[i]);
        }
        return results$;
      },
      set: function(it){
        this.off();
        this.pos = it;
        if (model.grille[it % 7] == 0) {
          this.on();
        }
        return it;
      },
      on: function(){
        var ref$;
        if (o.animRun) {
          return false;
        }
        this.off();
        return o.grille[(((this.pos) % (ref$ = 7) + ref$) % ref$)] = model.getPlayer();
      }
    };
  }
  get_grid_data(){
    return{
      toto:5
    }
  }
  columnClick(event){
    this.fen.disp= '';
    this.whyItIsFinish= true;
    this.popup.deplier(false);
    event.stopPropagation()
  }

  getSaveStyle(){
    return {}
  }

  reverseIsBotActive(){
    this.reverse('isBotActive');
    if (model.backup.length % 2 === 0) {
      return this.undo();
    }
  }

  // find_element(selector){
  //   this._elementRef.nativeElement.querySelector(selector)
  // }

  touchStart(event){
    this.displayScroll = 'block';
    this.lastY = event.touches[0].clientY;
    return this.touchMove();
  }

  touchMove(event = null){
    var pos;
    if (this.popup.isDisp()) {
      return false;
    }
    pos = event.touches.length >= 1
      ? event.touches[0].pageX
      : -1;
    if (pos < 0) {
      return false;
    }
    const p4 =  <HTMLElement> document.querySelector('#p4')
    this.preview.set( (pos - p4.offsetLeft |0) * 7 / p4.clientWidth );
    return event.preventDefault();
  }

  update_val(name,$event){
      this[name] = +$event.value
  }

  touchEnd(){
    if (this.popup.isDisp()) {
      return true;
    }
    event.preventDefault();
    return this.fallenPion(this.preview.pos);
  }

  ngOnInit() {
    this.init();
    this.popup.deplier(true);
    const model = ia.model;
    this.grille = model.grille.slice();
    this.popup.deplier(false);
    this.threadIsntUsed = true;
    model.playAgain();
    ia.boolSmart = 1;
    this.endGameMessage = false;
    this.fen.disp = 'option';
    this.grille = model.grille.slice();
  }

  replayIcon(){
    return isGameOver() && !this.whyItIsFinish || this.fen.disp === 'message';
  }
  darkWinningPos(dark){
    var f, colorNumber, this$ = this;
    f = model.winInfo;
    colorNumber = model.grille[f[0]] * (dark ? 4 : 1);
    return f.forEach(function(pos){
      return this$.grille[pos] = colorNumber;
    });
  }
  fullscreen(){
    this.fen.disp = 'play';
    this.popup.deplier(true);
    return this.whyItIsFinish = false;
  }
  messageIfEndGame(){
    if (model.weHaveAWinner()) {
      this.darkWinningPos(true);
      this.message = model.isHumanTurn() ? 'bravo vous avez gagné' + (2 * ia.boolSmart === model.backup.length ? ($(document).trigger('historiqueJeu', [model.backup]), 'envoyer votre historique par commentaire pour améliorer le jeu') : 'augmentez un peu le niveau') : 'Le robot gagne cette fois vous pouvez baisser le niveau de difficulté de quelques pourcents';
    } else if (model.isGameFull()) {
      this.message = 'ceci est une égalité mais pas une victoire';
    } else {
      return false;
    }
    this.fen.disp = 'message';
    this.whyItIsFinish = false;
    this.popup.deplier(true);
    return true;
  }

  anim(To, player){
    return new Promise((resolve) => {
      let i : number, asyncronousFor : Function;
      this.animRun = true;
      i = To % 7;
      return (asyncronousFor = ()=>{
        return setTimeout(i <= To ? ()=>{
          if (i > 6) {
            this.grille[i - 7] = 0;
          }
          this.grille[i] = player;
          i += 7;
          return asyncronousFor();
        } : resolve, this.time);
      })();
    });
  }

  fallenPion(pos){
    var n, ref$;
    if (this.modeCreator) {
      n = this.keyCode % 48;
      return this.grilleCreator[pos] = (function(){
        switch (false) {
        case !(-1 < n && n < 10):
          return n;
        default:
          return String.fromCharCode(this.keyCode).toLowerCase();
        }
      }.call(this));
    } else if (isGameOver()) {
      this.whyItIsFinish = false;
      this.popup.deplier(true);
      this.fen.disp = "";
      return this.stackPosition.length = 0;
    } else {
      (ref$ = this.stackPosition)[ref$.length] = pos;
      return this.loopThreatAnimation();
    }
  }

  loopThreatAnimation(){
    var pos, this$ = this;
    if (!(this.stackPosition.length && this.threadIsntUsed)) {
      return;
    }
    if (isGameOver()) {
      this.stackPosition.length = 0;
      this.threadIsntUsed = true;
      return;
    }
    this.threadIsntUsed = false;
    this.time = this.animation2 ? 50 : 0;
    pos = model.play(this.stackPosition.shift());
    if (pos < 0) {
      return this.threadIsntUsed = true;
    }
    return this.anim(pos, model.getPlayer(1)).then(function(){
      var posBot;
      this$.animRun = false;
      if (this$.messageIfEndGame()) {
        return;
      }
      if (this$.isBotActive && model.backup.length % 2 === 0) {
        posBot = ia.p4BlockEasy(pos, false);
        return this$.anim(posBot, 1);
      }
    }).then(function(){
      var ref$;
      this$.grille = model.grille.slice()
      this$.threadIsntUsed = true;
      if (this$.isBotActive) {
        this$.messageIfEndGame();
      }
      this$.animRun = false;
      return this$.loopThreatAnimation();
    });
  }

  clickOnBlack(){
    this.fen.disp = 'play';
    return this.popup.deplier(false);
  }
  reverse(aString){
    return this[aString] = !this[aString];
  }
  alert(text){
    return alert(text);
  }
  optionDisp(){
    // if (this.optionsWidth === optionsWidthInit) {
    //   return 'block';
    // } else {
      return 'none';
    // }
  }
  loadStory(grille2){
    if (!grille2) {
      return false;
    }
    model.restore(grille2);
    if (!(model.backup.length % 2)) {
      this.undo();
    }
    model.grille.forEach((val, i) => {
      this.grille[i] = val
    })
    return model.setPlayer(2);
  }
  load(grille2){
    model.setModel(grille2);
    this.grille.forEach((val, i) => {
      this.grille[i] = val
    })
  }
  modeleCreator(){
    var ref$;
    if (!this.modeCreator) {
      this.keyCode = 5 + 96;
      this.grilleCreator = this.grille.slice();
      this.grilleCreator[(ref$ = model.backup)[ref$.length - 1]] = 0;
    }
    return this.reverse('modeCreator');
  }
  goodGrille(){
    if (this.modeCreator) {
      return this.grilleCreator;
    } else {
      return this.grille;
    }
  }
  displayOption(){
    if (!(this.endGameMessage && this.popup.isDisp() || this.replayIcon())) {
      this.popup.deplier();
    }
    this.whyItIsFinish = true;
    this.endGameMessage = false;
    return this.fen.disp = 'option';
  }
  init(){
    var this$ = this;
    this.popup.deplier(false);
    this.threadIsntUsed = true;
    model.playAgain();
    ia.boolSmart = 1;
    this.endGameMessage = false;
    this.fen.disp = 'option';
    this.grille = model.grille.slice();
    return this.replayIconPop = new ShowHideSwitch(function(){
      return this$.replayIcon();
    });
  }

  restore(){
  //  this.loadStory(borto.cookies.getTab('backup'));
    return ia.boolSmart //= borto.cookies.getTab('boolSmart');
  }
  save(){
  //  borto.cookies.set('backup', model.backup);
    return ia.boolSmart //borto.cookies.set('boolSmart', ia.boolSmart);
  }
  graphique(colorNumber){
    return this.tabColor[colorNumber];
  }
  undo(){
    var pos;
    if (isGameOver()) {
      this.fen.disp = '';
      this.popup.deplier(false);
      this.darkWinningPos(false);
      if (this.whyItIsFinish) {
        this.whyItIsFinish = false;
      } else {
        model.weHaveAWinner(false);
      }
    }
    this.threadIsntUsed = true;
    pos = model.undo();
    this.grille[pos] = 0;
    if (this.isBotActive && model.getPlayer() === 1) {
      if (model.backup.length % 2 === 0) {
        this.undo();
      } else {
        model.setPlayer(2);
      }
      if (ia.boolSmart > 1) {
        ia.boolSmart--;
      }
    }
    return this.popup.deplier(false);
  }
  keydown(e){
    var that, ref$, n;
    e.preventDefault();
    this.keyCode = e.which;
    if (!this.modeCreator) {
      switch (that = e.which) {
      case 37:
      case 39:
        return this.preview.add(that - 38);
      case 38:
      case 90:
        return this.undo();
      case 40:
        return this.fallenPion(this.preview.pos);
      default:
        if (0 <= (ref$ = n = that % 48) && ref$ < 10) {
          return this.fallenPion(n);
        }
      }
    }
  }

  // goodGrille(){
  //   return this.grille
  // }

}
