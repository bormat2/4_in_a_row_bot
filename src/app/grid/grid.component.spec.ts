import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { GridComponent } from './grid.component';
import { Component, CUSTOM_ELEMENTS_SCHEMA,getDebugNode,DebugElement } from '@angular/core';
import {By} from '@angular/platform-browser'

describe('GridComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        GridComponent
      ],
    }).compileComponents();
  }));

  it('should create the Grid', () => {
    const fixture = TestBed.createComponent(GridComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('Check the existance  and color of cases', () => {
    const fixture = TestBed.createComponent(GridComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const img = compiled.querySelector('span:nth-child(39) > img')
    expect(!!img).toBeTruthy()
    expect(img.style.backgroundColor).toBe('rgb(255, 251, 0)')
    expect(compiled.querySelector('span:nth-child(41) > img').backgroundColor).toBe(undefined)
  });

  // it('should render title in a h1 tag', () => {
  //   const fixture = TestBed.createComponent(GridComponent);
  //   fixture.detectChanges();
  //   setTimeout(()=>{
  //     // const compiled = fixture.debugElement.nativeElement;
  //     // let img = compiled.querySelector('span:nth-child(39) > img')
  //     // console.log(compiled.outerHTML)
  //     // let fixture = TestBed.createComponent(TestComponent);
  //     fixture.detectChanges();
  //     const shadowRoot: DocumentFragment = fixture.debugElement.nativeElement.shadowRoot;
  //     if(!shadowRoot){
  //       throw 'shadowRoot is null'
  //     }
  //     const nestedComponentNativeElement = shadowRoot.querySelector('app-grid');
  //     const nestedComponentDebugElement = <DebugElement>getDebugNode(nestedComponentNativeElement);
  //     var nestedComponentInstance = nestedComponentDebugElement.componentInstance;
  //     // here can be your code
  //     let component = fixture.componentInstance;
  //
  //     component.state = 2;
  //     fixture.detectChanges();
  //     let de = nestedComponentDebugElement.query(By.css('span:nth-child(39)'));
  //
  //     expect(de.nativeElement.outerHTML).toBe('Nested component - 2');
  //     // expect(img.style.backgroundColor).toEqual('rgb(255, 251, 0)');
  //   },1000)
  //
  // });

});
