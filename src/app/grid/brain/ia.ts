// import * as model from './modele';
import prototype from './prototype';
import model from './modele';

var mod = function(a, n) {
	return a - Math.floor(a/n) * n;
}

var i_debug = 0;

declare global {
  interface Number {
    sign(n: Number): Number;
    between(a: Number,b: Number): boolean;
  }
  interface Array<T> {
    at(a: Number): any;
  }
  interface String {
    reverse(): string;
  }
}
String.prototype.reverse = function() {
    var s = "";
    var i = this.length;
    while (i>0) {
        s += this.substring(i-1,i);
        i--;
    }
    return s;
}


Number.prototype.sign = function(n) {
	return (this>=0) ? 1 : -1;
}

Number.prototype.between= function(a : Number,b : Number){
	return (a<=this && b>=this);
}

Array.prototype.at = function(a : number){
	if (a<0) a+=this.length;
	return this[a];
}


// console.log(model)
import {attaque,defense,tabException,perfectModele,miniDef,mesModele,modPosDeconseille,interditUnPeu,TabWontBecomeLikeThisModelPlayerTurn} from './tableauDeModele.js'
  var IA, this$ = this;
  IA = {
    model,
    playAt: -1,
    dif: 100,
    boolSmart: 1,
    found: false,
    winningRedPairs: [],
    winningYellowOdds: [],
    winningRedOdds: [],
    winningYellowPairs: [],
    forbids: [],
    inadvisables: [],
    pos: -1,
    modelId: 0,
    p4BlockEasy: function(posJoueur, retournerPosition){
      var findAt, botSmart, i$, i;
      // window.break2 = window['break'] = 0;
      IA.posJoueur = posJoueur;
      if (model.weHaveAWinner()) {
        return false;
      }
      if (parseInt(IA.dif) / 100 + Math.random() > 1) {
        IA.boolSmart++;
        model.setPlayer(1);
        IA.fillsWinningPos();
        IA.pos = -1;
        IA.modelId = 0;
        findAt = {
          modelID: 0
        };
        IA.posModBot = 48;
				let stop_when_we_know_where_to_play = function(func){
          func();
          return ~IA.pos;
        }

        let algo_by_priority = [
          () => IA.gagnerDirect(),
        	() => IA.bloquerDirect(),
        	() => IA.findForbiddenAndNotRecommandedPosition(),
        	() => IA.winInTwoTurn(1),
        	() => {
            IA.modelId = 0;
            return IA.modeledetectorAndAnswer(perfectModele);
          },
					() => {
            IA.modelId = 0;
            IA.posModBot = 48;
            return IA.playWithModel();
          },
					() => IA.playWithoutModel(posJoueur)
        ]
				let last_i = 0;
				algo_by_priority.some((element, i) => {
					last_i = i
					return !!stop_when_we_know_where_to_play(element)
				});
				// console.log(last_i)
      } else {
        for (i$ = 0; i$ <= 6; ++i$) {
          i = i$;
          IA.pos = ~~(Math.random() * 7);
          if (!model.grille[IA.pos]) {
            break;
          }
        }
      }
      return model.play(IA.pos, retournerPosition);
    },
    playWithModel: function(){
      var j, rec;
      j = 0;
      IA.posModBot = 48;
      IA.modelId = 0;
      return (rec = function(){
        var TabOfTab, position3;
        TabOfTab = [attaque, defense, miniDef, mesModele];
        while (j < TabOfTab.length) {
          for (;;) {
            if (!(IA.modelId < TabOfTab[j].length)) {
              break;
            }
            position3 = IA.modeledetectorAndAnswer(TabOfTab[j]);
            IA.modelId;
						++i_debug;
						// console.log(i_debug)
            if (~position3) {
              IA.ifPlayHereGiveMeExactPos(position3);
              if (~IA.pos) {
                IA.wontBecomeLikeThisModel(TabWontBecomeLikeThisModelPlayerTurn, 1, IA.pos);
                if (!IA.found) {
                  rec();
                }
                return IA.pos;
              }
              IA.posModBot--;
            }
            IA.modelId++;
          }
          IA.modelId = 0;
          IA.posModBot = 48;
          j++;
        }
        IA.posModBot = "notFound";
        return -1;
      })();
    },
    notUnderMe: function(inadvisables){
      var i$, i, position, results$ = [];
      for (i$ = 0; i$ <= 6; ++i$) {
        i = i$;
        model.nextPlayer();
        position = model.play(i, true);
        model.grille[position] = 2;
        model.nextPlayer();
        model.play(i, true);
        model.grille[position] = 0;
        if (model.weHaveAWinner()) {
          if (position !== inadvisables.at(-1) && position >= 0) {
            if (position >= 0) {
              results$.push(inadvisables.push(position));
            }
          }
        }
      }
      return results$;
    },
    ifPlayHereGiveMeExactPos: function(posJoueur){
      var cond1, cond2;
      posJoueur = +posJoueur;
      IA.pos = model.play(posJoueur, true);
      if (~IA.pos) {
        cond1 = ~IA.forbids.indexOf(IA.pos);
        cond2 = ~IA.inadvisables.indexOf(IA.pos);
        if (!(cond1 || cond2)) {
          IA.pos = model.play(IA.pos, true);
          return IA.pos;
        }
      }
      return IA.pos = -1;
    },
    positionOfSym: function(pos, length, sym){
      var ref$;
      return pos += sym && mod(length + ~pos, 7) - pos % 7;
    },
    fillsWinningPos: function(){
      var i$, o, trouvePlayerImpaire, trouveBotPaire, trouvePlayerPaire, trouveBotImpaire, results$ = [];
      for (i$ = 0; i$ <= 6; ++i$) {
        o = i$;
        IA.winningYellowPairs[o] = -1;
        IA.winningRedPairs[o] = -1;
        IA.winningYellowOdds[o] = -1;
        IA.winningRedOdds[o] = -1;
      }
      for (i$ = 0; i$ <= 41; ++i$) {
        o = i$;
        if (model.grille[o] == 0) {
          model.grille[o] = 1;
          trouvePlayerImpaire = IA.winningYellowOdds[o % 7];
          trouveBotPaire = IA.winningRedPairs[o % 7];
          trouvePlayerPaire = IA.winningYellowPairs[o % 7];
          trouveBotImpaire = IA.winningRedOdds[o % 7];
          if (Math.floor(o / 7) % 2 == 1 && trouvePlayerImpaire == -1 && model.weHaveAWinner(o)) {
            IA.winningYellowOdds[o % 7] = o;
          } else {
            if (Math.floor(o / 7) % 2 == 0 && trouvePlayerPaire == -1 && model.weHaveAWinner(o)) {
              IA.winningYellowPairs[o % 7] = o;
            }
          }
          model.grille[o] = 2;
          if (Math.floor(o / 7) % 2 == 0 && trouveBotPaire == -1 && model.weHaveAWinner(o)) {
            IA.winningRedPairs[o % 7] = o;
          } else {
            if (Math.floor(o / 7) % 2 == 1 && trouveBotImpaire == -1 && model.weHaveAWinner(o)) {
              IA.winningRedOdds[o % 7] = o;
            }
          }
          model.grille[o] = 0;
        }
        results$.push(null);
      }
      return results$;
    },
    playAllPos: function(u){
      var i$, to$;
      IA.pos = -1;
      for (i$ = u, to$ = u + 6; i$ <= to$; ++i$) {
        u = i$;
        IA.ifPlayHereGiveMeExactPos(u);
        if (~IA.pos) {
          return true;
        }
      }
      return false;
    },
    playWithoutModel: function(){
      var firstTurn;
      firstTurn = true;
      do {
        IA.playAllPos(IA.posJoueur);
        if (IA.pos > 0 && firstTurn) {
          do {
            IA.wontBecomeLikeThisModel(TabWontBecomeLikeThisModelPlayerTurn, 1, IA.pos);
            if (IA.found) {
              return;
            }
          } while (IA.playAllPos(IA.posJoueur));
          firstTurn = false;
        }
      } while ((IA.inadvisables.pop() != null || IA.forbids.pop() != null) && IA.pos < 0);
      return null;
    },
    winInTwoTurn: function(playerTurn){
      var i$, i, position2, cptGagnerDirect, j$, o, WinnerPos, otherPlayerWinOnMe;
      for (i$ = 0; i$ <= 6; ++i$) {
        i = i$;
        model.setPlayer(playerTurn);
        IA.pos = model.play(i, true);
        if (playerTurn == 1
          ? IA.forbids.indexOf(IA.pos) < 0
          : !IA.comparerLigne("g", IA.pos - 7)) {
          position2 = IA.pos;
          model.grille[IA.pos] = playerTurn;
          cptGagnerDirect = 0;
          for (j$ = 0; j$ <= 6; ++j$) {
            o = j$;
            IA.pos = model.play(o, true);
            if (model.weHaveAWinner() && ~IA.pos) {
              cptGagnerDirect++;
              WinnerPos = model.getPlayer() == 1 ? "g" : "i";
              otherPlayerWinOnMe = model.getPlayer() == 1
                ? false
                : IA.comparerLigne("g", IA.pos);
              if ((cptGagnerDirect == 1 && IA.comparerLigne(WinnerPos, IA.pos - 7)) || (cptGagnerDirect > 1 && !otherPlayerWinOnMe)) {
                model.grille[position2] = 0;
                IA.pos = i;
                return i;
              }
            }
          }
          model.grille[position2] = 0;
        }
      }
      return IA.pos = -1;
    },
    gagnerDirect: function(){
      var i$, i;
      for (i$ = 0; i$ <= 7; ++i$) {
        i = i$;
        IA.pos = model.play(i, true);
        if (model.weHaveAWinner()) {
          model.weHaveAWinner(false);
          return true;
        }
      }
      return IA.pos = -1;
    },
    wontBecomeLikeThisModel: function(TabWontBecomeLikeThisModel, player, posBot){
      var i$, i, pos, j$, len$, mod;
      posBot = model.play(posBot, true);
      model.grille[posBot] = player;
      for (i$ = 0; i$ <= 6; ++i$) {
        i = i$;
        pos = model.play(i, true);
        model.grille[pos] = 2;
        if (~pos) {
          for (j$ = 0, len$ = TabWontBecomeLikeThisModel.length; j$ < len$; ++j$) {
            mod = TabWontBecomeLikeThisModel[j$];
            IA.found = !!IA.structModelDetector2(mod, 48);
            if (IA.found) {
              break;
            }
          }
          model.grille[pos] = 0;
          if (IA.found) {
            IA.inadvisables.push(parseInt(posBot));
            break;
          }
        }
      }
      model.grille[posBot] = 0;
      return IA.found = !IA.found;
    },
    futureIWant: function(a, ModelInStruct, pos){
      var i$, j, pos3;
      for (i$ = 0; i$ <= 6; ++i$) {
        j = i$;
        if (IA.found) {
          break;
        }
        pos3 = model.play(j, true);
        if (~pos3) {
          model.grille[pos3] = 1;
          IA.found = IA.findModel2(ModelInStruct, pos);
          model.grille[pos3] = 0;
        }
      }
      if (IA.found) {
        IA.currMod = ModelInStruct.tab;
        IA.playAt = pos3;
      }
      return IA.found;
    },
    modeledetectorAndAnswer: function(modele){
      var tab;
      IA.modele = modele;
      tab = [];
      while (IA.modelId < modele.length) {
        tab = IA.getListOfMatchingPos();
        IA.modelId++;
        if (IA.found) {
          break;
        }
      }
      IA.modelId--;
      return IA.pos = IA.found
        ? tab[0]
        : -1;
    },
    findModel2: function(ModelInStruct, pos){
      var otherOption, stringToEVal, logicalOperator, i$, to$, i, length, j;
      IA.currMod = ModelInStruct.tab;
      IA.found = false;
      if (!ModelInStruct.logicalOperator) {
        return IA.currMod.some(function(mod){
          return IA.found = IA.modeleDetector3(mod, pos);
        });
      } else {
        otherOption = {};
        stringToEVal = '';
        logicalOperator = ModelInStruct.logicalOperator;
        for (i$ = 0, to$ = logicalOperator.length - 2; i$ <= to$; ++i$) {
          i = i$;
          stringToEVal += logicalOperator[i] + ("IA.modeleDetector3(ModelInStruct.tab[" + i + "],pos,otherOption)");
        }
        stringToEVal += logicalOperator[logicalOperator.length - 1];
        length = ModelInStruct.sym ? 1 : 2;
        for (i$ = 0; i$ < length; ++i$) {
          j = i$;
          if (ModelInStruct.hasOwnProperty('sameSym') && length == 2) {
            otherOption.sym = !!j;
          }
          IA.found = eval(stringToEVal);
        }
        return IA.found;
      }
    },
    structModelDetector2: function(ModelInStruct, pos){
      var posMod, exept;
      window['break'] = window['break'] || 0;
      window['break']++;
      IA.playAt = -1;
      IA.found = IA.findModel2(ModelInStruct, pos);
      if (ModelInStruct.mode == 'futur') {
        if (IA.found) {
          IA.playAt = -1;
          IA.found = !IA.found;
        } else {
          IA.found = IA.futureIWant({}, ModelInStruct, pos);
        }
      }
      posMod = IA.posMod;
      if (IA.found) {
        IA.struct = ModelInStruct;
        exept = ModelInStruct.exept;
        if (exept) {
          exept.sym = IA.sym;
          IA.found = IA.structModelDetector2(exept, 48);
          IA.currMod = ModelInStruct.tab;
          IA.playAt = IA.found && -1 || true;
          IA.found = !IA.found;
        }
        if (IA.found && ModelInStruct.playAt != null) {
          IA.playAt = ModelInStruct.playAt;
        }
      }
      IA.posMod = posMod;
      return IA.found;
    },
    bloquerDirect: function(){
      var i$, i;
      model.nextPlayer();
      for (i$ = 0; i$ <= 6; ++i$) {
        i = i$;
        IA.pos = model.play(i, true);
        if (model.weHaveAWinner()) {
          model.nextPlayer();
          return IA.pos = model.play(IA.pos, true);
        }
      }
      model.nextPlayer();
      return IA.pos = -1;
    },
    dontHelpJ2: function($forbids){
      var i$, i, position, results$ = [];
      for (i$ = 0; i$ <= 6; ++i$) {
        i = i$;
        position = model.play(i, true);
        model.grille[position] = 1;
        model.nextPlayer();
        model.play(i, true);
        model.nextPlayer();
        model.grille[position] = 0;
        if (model.weHaveAWinner() && position !== $forbids[$forbids.length - 1] && position >= 0) {
          results$.push($forbids.push(position));
        }
      }
      return results$;
    },
    detectBadPositionAlgorythme: function(){
      var i$, i, pos;
      for (i$ = 0; i$ <= 6; ++i$) {
        i = i$;
        model.setPlayer(1);
        pos = model.play(i, true);
        model.grille[pos] = '1';
        if (~IA.winInTwoTurn(2)) {
          IA.inadvisables.push(pos);
        }
        model.grille[pos] = 0;
      }
      return model.setPlayer(1);
    },
    getListOfMatchingPos: function(){
      var addPosOkToGroup, tabPosInBigGrille, model, posRelativeToModele, ref$;
      addPosOkToGroup = function(posRelativeToModele){
        return IA.posMod + IA.positionOfSym(posRelativeToModele, IA.currMod[0].length, IA.sym);
      };
      tabPosInBigGrille = [];
      model = IA.modele[IA.modelId];
      IA.found = IA.structModelDetector2(model, IA.posModBot);
      IA.pos = IA.posMod;
      IA.currMod = IA.currMod[0];
      if (IA.found) {
        tabPosInBigGrille = Array.isArray(IA.playAt)
          ? (function(){
            var i$, ref$, len$, results$ = [];
            for (i$ = 0, len$ = (ref$ = IA.playAt).length; i$ < len$; ++i$) {
              posRelativeToModele = ref$[i$];
              results$.push(addPosOkToGroup(posRelativeToModele));
            }
            return results$;
          }())
          : [((ref$ = IA.struct) != null ? ref$.mode : void 8) == 'futur'
            ? IA.playAt
            : addPosOkToGroup(IA.playAt)];
        IA.posModBot = IA.beginToEnd(IA.posMod);
      } else {
        IA.posModBot = 48;
      }
      return tabPosInBigGrille;
    },
    beginToEnd: function(begin){
      return begin + IA.currMod.length * 7 - 7;
    },
    findForbiddenAndNotRecommandedPosition: function(){
      var tabForbids, i$, len$, tab;
      IA.forbids.length = IA.inadvisables.length = 0;
      IA.dontHelpJ2(IA.forbids);
      IA.detectBadPositionAlgorythme();
      IA.posModBot = 48;
      tabForbids = [modPosDeconseille, interditUnPeu];
      for (i$ = 0, len$ = tabForbids.length; i$ < len$; ++i$) {
        IA.modele = tabForbids[i$];
        IA.modelId = 0;
        while (IA.modelId < IA.modele.length) {
          tab = IA.getListOfMatchingPos();
          if (!IA.found) {
            IA.posModBot = 48;
            IA.modelId++;
          } else {
						++i_debug;

            Array.prototype.push.apply(IA.inadvisables, tab);
            IA.posModBot--;
          }
        }
      }
      IA.notUnderMe(IA.inadvisables);
      return IA.DeleteException();
    },
    DeleteException: function(){
      var position, pos, results$ = [];
      IA.modelId = 0;
      IA.posModBot = 48;
      while (IA.modelId < mesModele.length) {
        IA.modeledetectorAndAnswer(tabException);
        if (~IA.pos) {
          position = IA.pos;
          pos = IA.inadvisables.indexOf(position);
          while (~pos) {
            IA.inadvisables.splice(pos, 1);
            pos = IA.inadvisables.indexOf(position);
          }
          results$.push(IA.posModBot--);
        } else {
          break;
        }
      }
      return results$;
    },
    modeleDetector3: function(oneModele, pos, otherOption){
      var sym, dontChangeSym, posOneModeleSym, stopLoopCond, poses, isModelfound;
      sym = true;
      dontChangeSym = false;
      otherOption = otherOption || {};
      if (otherOption.hasOwnProperty('sym')) {
        sym = otherOption.sym;
        dontChangeSym = true;
      }
      posOneModeleSym = {
        'true': {
          pos: pos
        },
        'false': {
          pos: pos
        }
      };
      stopLoopCond = function(){
        var posSym, pos;
        posSym = posOneModeleSym[sym].pos;
        pos = posOneModeleSym[!sym ? 'true' : 'false'].pos;
        return posOneModeleSym[sym].isModelfound || otherOption.hasOwnProperty('samepos') || posSym < 0 && pos < 0 || dontChangeSym && posSym < 0;
      };
      for (;;) {
        poses = posOneModeleSym[sym] = IA.modeleDectector1(oneModele, posOneModeleSym[sym].pos, sym);
        if (!poses.isModelfound) {
          poses.pos = Math.min(Math.ceil(poses.pos / 7) * 7 - oneModele[0].length, poses.pos - 1);
          sym = !dontChangeSym && !sym;
        }
        if (!!stopLoopCond()) {
          break;
        }
      }
      IA.posMod = IA.playAt = -1;
      isModelfound = posOneModeleSym[sym].isModelfound;
      if (isModelfound) {
        IA.posMod = posOneModeleSym[sym].pos - 7 * (oneModele.length - 1);
        IA.playAt = isModelfound;
        IA.sym = sym;
      }
      return isModelfound;
    },
    modeleDectector1: function(oneModele, posOneModele, sym){
      var i$, to$, i, line;
      // window.break2 = window.break2 || 0;
      // window.break2++;
      for (i$ = 1, to$ = oneModele.length; i$ <= to$; ++i$) {
        i = i$;
        line = oneModele[oneModele.length - i];
        if (sym) {
          line = line.reverse();
        }
        if (!IA.comparerLigne(line, posOneModele - 7 * (i - 1))) {
          return {
            pos: posOneModele
          };
        }
      }
      return {
        pos: posOneModele,
        isModelfound: true
      };
    },
    topToBottom: function(pos, length){
      return pos + length * 7 - 7;
    },
    bottomToTop: function(pos, length){
      return pos + ~length * 7;
    }
  };
  IA.comparerCaractere = function(a, car, impaire){
    car = +car;
    if (isNaN(a)) {
      return a == 'a' || a == 'y' && car !== 2 || a == 'z' && car !== 1;
    } else {
      a = +a;
      return car == a || a == 9 && car !== 0 || a == 6 && (car === 1 || car === 2) || a == 5 && (car === 1 || car === 2 || car === 0) || (impaire
        ? a == 8 && car == 2 || a == 4 && car == 1
        : car == 1 && a == 3 || a == 7 && car == 2);
    }
  };
  IA.comparerLigne = function(modligne, o){
    var cont, i, a, b, impaire;
    if (o < 0) {
      return false;
    }
    cont = true;
    i = o;
    while (i < modligne.length + o && cont) {
      if (o < 0) {
        return false;
      }
      a = modligne.charAt(i - o);
      b = model.grille[i];
      impaire = Math.floor(i / 7) % 2;
      cont = null;
      if (model.grille[i] == 0) {
        switch (false) {
        case !in$(a, 'pq'):
          cont = IA.winningRedPairs[i % 7] < IA.winningYellowOdds[i % 7];
          break;
        case a != 't':
          cont = IA.inadvisables.indexOf(i) + 1;
          break;
        case a != 'r':
          cont = IA.winningRedPairs[i % 7] < IA.winningYellowOdds[i % 7] || IA.winningYellowOdds[i % 7] == -1;
          cont && (cont = IA.winningRedPairs[i % 7] <= i);
          model.grille[i] = 1;
          cont && (cont = model.weHaveAWinner(i));
          break;
        case !in$(a, 'f.'):
          model.grille[i] = 1;
          cont = model.weHaveAWinner(i);
          if ('f' == a) {
            cont = !cont;
          }
          if (!cont) {
            model.grille[i] = 2;
            cont = model.weHaveAWinner(i);
          }
          break;
        case !in$(a, 'erw'):
          cont = IA.winningRedPairs[i % 7] < IA.winningYellowOdds[i % 7] || IA.winningYellowOdds[i % 7] == -1;
          cont = cont && IA.winningRedPairs[i % 7] <= i;
          break;
        case !in$(a, 'gj'):
          model.grille[i] = 1;
          cont = model.weHaveAWinner(i);
          break;
        case !in$(a, 'hi'):
          model.grille[i] = 2;
          cont = model.weHaveAWinner(i);
        }
        if (in$(a, 'hpwfj')) {
          cont = !cont;
        }
        model.grille[i] = 0;
      }
      cont == null && (cont = IA.comparerCaractere(a, b, impaire));
      if (cont && i > modligne.length - 2 + o) {
        return cont;
      }
      i++;
    }
  };
  function in$(x, xs){
    var i = -1, l = xs.length >>> 0;
    while (++i < l) if (x === xs[i]) return true;
    return false;
  }

export default IA
