"use strict";
declare global {
  interface Number {
    sign(n: Number): Number;
    between(a: Number,b: Number): boolean;
  }
  interface Array<T> {
    at(a: Number): any;
  }
}


Number.prototype.sign = function(n) {
	return (this>=0) ? 1 : -1;
}

Number.prototype.between= function(a : Number,b : Number){
	return (a<=this && b>=this);
}

Array.prototype.at = function(a : number){
	if (a<0) a+=this.length;
	return this[a];
}

export default {}
