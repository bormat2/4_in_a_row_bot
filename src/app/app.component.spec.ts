import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { GridComponent } from './grid/grid.component';
import { Component, CUSTOM_ELEMENTS_SCHEMA,getDebugNode,DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser'

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    // expect(app).toBeTruthy();
  });

  it(`should have as title 'four-in-a-row'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('four-in-a-row');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to four-in-a-row!');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    var comp = fixture.componentInstance;

    // const app = fixture.debugElement.componentInstance as AppComponent;
    const debugElement = fixture.debugElement.queryAll(By.css('app-grid'))[0]
    fixture.detectChanges();
    console.log(debugElement.nativeElement.querySelector('*'))



    // // app.active = true;
    // app.title = 'Title';
    // expect(shadowRoot.querySelector('.pion').textContent).toEqual('Title');
  })

  // it('should render title in a h1 tag', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   setTimeout(()=>{
  //     // const compiled = fixture.debugElement.nativeElement;
  //     // let img = compiled.querySelector('span:nth-child(39) > img')
  //     // console.log(compiled.outerHTML)
  //     // let fixture = TestBed.createComponent(TestComponent);
  //     fixture.detectChanges();
  //     const shadowRoot: DocumentFragment = fixture.debugElement.nativeElement.shadowRoot;
  //     if(!shadowRoot){
  //       throw 'shadowRoot is null'
  //     }
  //     const nestedComponentNativeElement = shadowRoot.querySelector('app-grid');
  //     const nestedComponentDebugElement = <DebugElement>getDebugNode(nestedComponentNativeElement);
  //     var nestedComponentInstance = nestedComponentDebugElement.componentInstance;
  //     // here can be your code
  //     let component = fixture.componentInstance;
  //
  //     component.state = 2;
  //     fixture.detectChanges();
  //     let de = nestedComponentDebugElement.query(By.css('span:nth-child(39)'));
  //
  //     expect(de.nativeElement.outerHTML).toBe('Nested component - 2');
  //     // expect(img.style.backgroundColor).toEqual('rgb(255, 251, 0)');
  //   },1000)
  //
  // });

});
