import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  // @ViewChild(any)
  @Output() valueChange : EventEmitter<Object>  = new EventEmitter();
  @Input() val : number = 0;

  constructor() {}

  onChange(event){
     this.valueChange.emit({
       value: event.target.value
     });
  }

  ngOnInit() {
  }

}
