import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'four-in-a-row';
  state : number;
  griddata : 'empty'

  set_grid_data(event){
    this.griddata = event
  }
}
